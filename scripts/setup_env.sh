case ${CI_COMMIT_REF_NAME} in
    "development")
        export ENVIRONMENT="development"
        export HEROKU_APP="instore-manager-register-dev"
        ;;
    "stage")
        export ENVIRONMENT="stage"
        export HEROKU_APP="instore-manager-register-stg"
        ;;
    "release")
        export ENVIRONMENT="release"
        export HEROKU_APP="nothing"
        ;;
esac
echo "Enviroment OK '${HEROKU_APP}'"
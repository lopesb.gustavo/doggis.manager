package com.puc.doggis.register.domain.pet;

import com.puc.doggis.register.configuration.enumerated.PetSizeType;
import com.puc.doggis.register.domain.client.Client;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Table(name = "pet")
public class Pet
{

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    @Setter
    private String name;

    @Column(name = "type")
    @Setter
    private String type;

    @Column(name = "race")
    @Setter
    private String race;

    @Enumerated(EnumType.ORDINAL)
    @Column(name = "pet_size")
    @Setter
    private PetSizeType petSizeType;

    @Column(name = "observations")
    @Setter
    private String observations;

    @Column(name = "can_take_pictures")
    @Setter
    private Boolean canTakePictures;

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "owner_id")
    private Client owner;

}

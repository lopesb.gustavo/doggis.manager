package com.puc.doggis.register.infraestructure.rest.client;

import com.puc.doggis.register.acl.wrapper.client.ClientWrapper;
import com.puc.doggis.register.acl.wrapper.commons.PageWrapper;
import com.puc.doggis.register.application.client.ClientService;
import com.puc.doggis.register.domain.client.Client;
import io.swagger.annotations.*;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import static java.util.Objects.isNull;

@RestController
@RequestMapping("client")
@AllArgsConstructor
public class ClientRestController
{

    private final ClientService clientService;

    @GetMapping(name="/", produces = "application/json")
    public PageWrapper retrieveAll(
            @RequestParam(value = "page",required = false) Integer page,
            @RequestParam(value = "pageSize", required = false) Integer pageSize,
            @RequestParam(value = "query", required = false) String searchData)
    {
        page = isNull(page)? 0 : page;
        pageSize = isNull(pageSize)? 25 : pageSize;
        Page clients = clientService.findAll(page,pageSize,searchData);
        return new PageWrapper(page, pageSize, Client.class, clients);
    }

    @RequestMapping(value="/{id}", method=RequestMethod.GET)
    public ClientWrapper retrieveClient(@PathVariable Long id)
    {
        return new ClientWrapper(clientService.findById(id));
    }

    @PostMapping(name = "/")
    @ResponseStatus(value = HttpStatus.CREATED)
    public ClientWrapper createClient(
            @ApiParam(hidden = true)
            @RequestBody ClientWrapper clientWrapper)
    {
        return new ClientWrapper(clientService.create(clientWrapper.getUser()));
    }

    @RequestMapping(value="/{id}", method=RequestMethod.PUT)
    public ClientWrapper updateClient(@PathVariable Long id, @ApiParam(hidden = true) @RequestBody ClientWrapper clientWrapper)
    {
        return new ClientWrapper(clientService.update(id, clientWrapper.getUser()));
    }
}

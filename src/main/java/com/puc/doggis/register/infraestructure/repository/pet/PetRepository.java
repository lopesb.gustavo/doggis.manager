package com.puc.doggis.register.infraestructure.repository.pet;

import com.puc.doggis.register.domain.pet.Pet;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface PetRepository extends PagingAndSortingRepository<Pet, Long>
{

    @Query("SELECT pet FROM Pet pet where (LOWER(pet.name) LIKE :query )")
    Page<Pet> findByQueryString(@Param("query") String query, Pageable pageable);

}

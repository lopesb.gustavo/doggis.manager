package com.puc.doggis.register.infraestructure.rest.pet;

import com.puc.doggis.register.acl.wrapper.commons.PageWrapper;
import com.puc.doggis.register.acl.wrapper.pet.PetWrapper;
import com.puc.doggis.register.application.pet.PetService;
import com.puc.doggis.register.domain.pet.Pet;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import static java.util.Objects.isNull;

@RestController
@RequestMapping("pet")
@AllArgsConstructor
public class PetRestController
{

    private final PetService petService;

    @GetMapping(name="/", produces = "application/json")
    public PageWrapper retrieveAll(
            @RequestParam(value = "page",required = false) Integer page,
            @RequestParam(value = "pageSize", required = false) Integer pageSize,
            @RequestParam(value = "query", required = false) String searchData)
    {
        page = isNull(page)? 0 : page;
        pageSize = isNull(pageSize)? 25 : pageSize;
        Page pets = petService.findAll(page,pageSize,searchData);
        return new PageWrapper(page, pageSize, Pet.class, pets);
    }

    @RequestMapping(value="/{id}", method=RequestMethod.GET)
    public PetWrapper retrievePet(@PathVariable Long id)
    {
        return new PetWrapper(petService.findById(id));
    }

    @PostMapping(name = "/")
    @ResponseStatus(value = HttpStatus.CREATED)
    public PetWrapper createPet(
            @ApiParam(hidden = true)
            @RequestBody PetWrapper PetWrapper)
    {
        return new PetWrapper(petService.create(PetWrapper.getPet()));
    }

    @RequestMapping(value="/{id}", method=RequestMethod.PUT)
    public PetWrapper updatePet(@PathVariable Long id, @ApiParam(hidden = true) @RequestBody PetWrapper PetWrapper)
    {
        return new PetWrapper(petService.update(id, PetWrapper.getPet()));
    }
}

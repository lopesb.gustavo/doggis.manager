package com.puc.doggis.register.application.client;

import static java.util.Objects.isNull;

import com.puc.doggis.register.configuration.exception.ResourceNotFoundException;
import com.puc.doggis.register.domain.client.Client;
import com.puc.doggis.register.infraestructure.repository.client.ClientRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class ClientApplicationService implements ClientService
{
    private final ClientRepository repository;

    public Client findById(Long id)
    {
        return repository.findById(id).orElseThrow(ResourceNotFoundException::new);
    }

    public Client create(Client user)
    {
        return repository.save(user);
    }

    public Client update(Long id, Client user)
    {
        Client actual = repository.findById(id).orElseThrow(ResourceNotFoundException::new);
        actual.setName(user.getName());
        actual.setEmail(user.getEmail());
        actual.setAddress(user.getAddress());
        actual.setRg(user.getRg());
        actual.setCpf(user.getCpf());
        actual.setPatazAmount(user.getPatazAmount());
        return repository.save(actual);
    }

    public Page<Client> findAll(Integer page, Integer pageSize, String query)
    {
        return isNull(query) ?
                    repository.findAll(PageRequest.of(page, pageSize)) :
                    repository.findByQueryString("%" + query.toLowerCase() + "%", PageRequest.of(page, pageSize));
    }

}

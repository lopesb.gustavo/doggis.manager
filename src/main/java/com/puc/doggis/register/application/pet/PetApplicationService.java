package com.puc.doggis.register.application.pet;

import com.puc.doggis.register.configuration.exception.ResourceNotFoundException;
import com.puc.doggis.register.domain.pet.Pet;
import com.puc.doggis.register.infraestructure.repository.pet.PetRepository;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import static java.util.Objects.isNull;

@Service
@AllArgsConstructor
public class PetApplicationService implements PetService
{
    private final PetRepository repository;

    public Pet findById(Long id)
    {
        return repository.findById(id).orElseThrow(ResourceNotFoundException::new);
    }

    public Pet create(Pet pet)
    {
        return repository.save(pet);
    }

    public Pet update(Long id, Pet pet)
    {
        Pet actual = repository.findById(id).orElseThrow(ResourceNotFoundException::new);
        actual.setName(pet.getName());
        actual.setType(pet.getType());
        actual.setRace(pet.getRace());
        actual.setPetSizeType(pet.getPetSizeType());
        actual.setCanTakePictures(pet.getCanTakePictures());
        actual.setObservations(pet.getObservations());
        return repository.save(actual);
    }

    public Page<Pet> findAll(Integer page, Integer pageSize, String query)
    {
        return isNull(query) ?
                    repository.findAll(PageRequest.of(page, pageSize)) :
                    repository.findByQueryString("%" + query.toLowerCase() + "%", PageRequest.of(page, pageSize));
    }

}

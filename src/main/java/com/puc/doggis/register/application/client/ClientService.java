package com.puc.doggis.register.application.client;

import com.puc.doggis.register.domain.client.Client;
import org.springframework.data.domain.Page;

public interface ClientService
{
    Client findById(Long id);

    Client create(Client role);

    Client update(Long id, Client role);

    Page<Client> findAll(Integer page, Integer pageSize, String query);

}

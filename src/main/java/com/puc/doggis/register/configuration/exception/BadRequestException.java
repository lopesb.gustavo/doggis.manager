package com.puc.doggis.register.configuration.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "O Conteúdo enviado não é compatível com o esperado")
public class BadRequestException extends RuntimeException {

}

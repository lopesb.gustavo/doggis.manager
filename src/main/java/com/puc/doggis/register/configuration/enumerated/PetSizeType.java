package com.puc.doggis.register.configuration.enumerated;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum PetSizeType
{
    SMALL("Pequeno"),
    MEDIUM("Médio"),
    BIG("Grande");

    private final String value;
}

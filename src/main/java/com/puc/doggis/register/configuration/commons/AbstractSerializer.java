package com.puc.doggis.register.configuration.commons;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import java.io.IOException;

public abstract class AbstractSerializer<T> extends StdSerializer<T>
{

    public AbstractSerializer()
    {
        this(null);
    }

    public AbstractSerializer(Class<T> t)
    {
        super(t);
    }

    public abstract void serialize(T t, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException;

    public void serializeObject(Object object, JsonGenerator jsonGenerator) throws IOException
    {
        throw new UnsupportedOperationException("Serialização não implementada");
    }

}

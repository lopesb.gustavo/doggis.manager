package com.puc.doggis.register.configuration.label.commons;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum PageLabel
{
    PAGE("page"),
    TOTAL_REGISTERS("totalRegisters"),
    ITENS_PER_PAGE("itensPerPage"),
    DATA("data");

    private final String value;

}

package com.puc.doggis.register.configuration.commons;

public interface ApplicationListener<T>
{
    public void onEventListening(T event);
}

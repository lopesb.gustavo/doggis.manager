package com.puc.doggis.register.acl.desserializer.user;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.puc.doggis.register.acl.wrapper.client.ClientWrapper;
import com.puc.doggis.register.configuration.commons.AbstractDeserializer;
import com.puc.doggis.register.configuration.enumerated.UserType;
import com.puc.doggis.register.configuration.label.client.ClientLabel;
import com.puc.doggis.register.domain.client.Client;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.io.IOException;

import static java.util.Objects.isNull;

@Service
@AllArgsConstructor
public class UserDeserializer extends AbstractDeserializer<ClientWrapper>
{

    @Override
    public ClientWrapper deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException
    {
        JsonNode node = jsonParser.readValueAsTree();
        Client client = deserializeObject(node, deserializationContext);

        return new ClientWrapper(client);
    }

    public Client deserializeObject(JsonNode node, DeserializationContext deserializationContext) throws IOException, JsonProcessingException
    {

        Long id = isNull(node.get(ClientLabel.ID.getValue()))? null : node.get(ClientLabel.ID.getValue()).asLong();
        JsonNode nameNode = node.get(ClientLabel.NAME.getValue());
        JsonNode emailNode = node.get(ClientLabel.EMAIL.getValue());
        JsonNode rgNode = node.get(ClientLabel.RG.getValue());
        JsonNode cpfNode = node.get(ClientLabel.CPF.getValue());
        JsonNode addressNode = node.get(ClientLabel.ADDRESS.getValue());
        JsonNode patazNode = node.get(ClientLabel.PATAZ_AMOUNT.getValue());

        return new Client(id, UserType.CLIENT, nameNode.asText(), emailNode.asText(), rgNode.asText(), cpfNode.asText(), addressNode.asText(), patazNode.asInt());
    }

}

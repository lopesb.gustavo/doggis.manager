package com.puc.doggis.register.acl.serializer.commons;

import java.io.IOException;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.puc.doggis.register.acl.wrapper.commons.PageWrapper;
import com.puc.doggis.register.configuration.commons.AbstractSerializer;
import com.puc.doggis.register.configuration.label.commons.PageLabel;

import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class PageSerializer extends AbstractSerializer<PageWrapper>
{

    private final Map<Class, AbstractSerializer> objectSerializers;

    @Override
    public void serialize(PageWrapper pageWrapper, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException
    {
        AbstractSerializer serializer = objectSerializers.get(pageWrapper.getEntity());
        jsonGenerator.writeStartObject();
        jsonGenerator.writeNumberField(PageLabel.PAGE.getValue(), pageWrapper.getPage());
        jsonGenerator.writeNumberField(PageLabel.ITENS_PER_PAGE.getValue(), pageWrapper.getItensPerPage());
        jsonGenerator.writeNumberField(PageLabel.TOTAL_REGISTERS.getValue(), pageWrapper.getTotalRegisters());
        jsonGenerator.writeFieldName(PageLabel.DATA.getValue());
        jsonGenerator.writeStartArray();
        for (Object object: pageWrapper.getData())
        {
            serializer.serializeObject(object,jsonGenerator);
        }
        jsonGenerator.writeEndArray();
        jsonGenerator.writeEndObject();
    }


}

package com.puc.doggis.register.acl.wrapper.pet;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.puc.doggis.register.acl.desserializer.pet.PetDeserializer;
import com.puc.doggis.register.acl.serializer.pet.PetSerializer;
import com.puc.doggis.register.domain.pet.Pet;
import lombok.AllArgsConstructor;
import lombok.Getter;

@JsonDeserialize(using = PetDeserializer.class)
@JsonSerialize(using = PetSerializer.class)
@AllArgsConstructor
@Getter
public class PetWrapper
{
    private final Pet pet;

}

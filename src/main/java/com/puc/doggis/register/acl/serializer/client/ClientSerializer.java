package com.puc.doggis.register.acl.serializer.client;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.puc.doggis.register.acl.wrapper.client.ClientWrapper;
import com.puc.doggis.register.configuration.commons.AbstractSerializer;
import com.puc.doggis.register.configuration.label.client.ClientLabel;
import com.puc.doggis.register.domain.client.Client;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
@AllArgsConstructor
public class ClientSerializer extends AbstractSerializer<ClientWrapper>
{

    @Override
    public void serialize(ClientWrapper userWrapper, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException
    {
        this.serializeObject(userWrapper.getUser(),jsonGenerator);
    }

    @Override
    public void serializeObject(Object object, JsonGenerator jsonGenerator) throws IOException
    {
        this.serializeObject((Client) object, jsonGenerator);
    }

    public void serializeObject(Client user, JsonGenerator jsonGenerator) throws IOException
    {
        jsonGenerator.writeStartObject();
        jsonGenerator.writeNumberField(ClientLabel.ID.getValue(), user.getId());
        jsonGenerator.writeStringField(ClientLabel.NAME.getValue(), user.getName());
        jsonGenerator.writeStringField(ClientLabel.EMAIL.getValue(), user.getEmail());
        jsonGenerator.writeStringField(ClientLabel.RG.getValue(), user.getRg());
        jsonGenerator.writeStringField(ClientLabel.CPF.getValue(), user.getCpf());
        jsonGenerator.writeStringField(ClientLabel.ADDRESS.getValue(), user.getAddress());
        jsonGenerator.writeNumberField(ClientLabel.PATAZ_AMOUNT.getValue(), user.getPatazAmount());
        jsonGenerator.writeEndObject();
    }
}

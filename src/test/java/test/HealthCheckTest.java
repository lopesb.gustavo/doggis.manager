package test;

import com.puc.doggis.register.infraestructure.RegisterApplication;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import commons.AbstractTest;

@RunWith (SpringRunner.class)
@SpringBootTest (classes = RegisterApplication.class)
@WebAppConfiguration
public class HealthCheckTest extends AbstractTest
{

    @Test
    public void testServiceUpAndRunning() throws Exception
    {
        mockMvc.perform(get("/healthCheck")).andExpect(status().isOk()).andExpect(content().string("Microserviço de Cadastros funcionando!"));
    }

}

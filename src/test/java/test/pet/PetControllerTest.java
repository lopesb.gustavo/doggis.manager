package test.pet;

import com.puc.doggis.register.infraestructure.RegisterApplication;
import commons.AbstractTest;
import commons.ClearContext;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith (SpringRunner.class)
@SpringBootTest (classes = RegisterApplication.class)
@ClearContext
@WebAppConfiguration
public class PetControllerTest extends AbstractTest
{

    @Test
    @ClearContext
    public void testCreateAndUpdatePet() throws Exception
    {
        String emptyPet = readFileAsString("outputs/pet/pet_initial.json");
        mockMvc.perform(get("/pet")).andExpect(status().isOk()).andExpect(content().string(emptyPet));

        String clientCreation = readFileAsString("inputs/client/client_creation.json");
        mockMvc.perform(post("/client").contentType(MediaType.APPLICATION_JSON).content(clientCreation)).andExpect(status().isCreated());

        String petCreation = readFileAsString("inputs/pet/pet_creation.json");
        String expectedCreationResponse = readFileAsString("outputs/pet/pet_creation_result.json");
        mockMvc.perform(post("/pet").contentType(MediaType.APPLICATION_JSON).content(petCreation)).andExpect(status().isCreated()).andExpect(content().json(expectedCreationResponse, false));

        String petUpdate = readFileAsString("inputs/pet/pet_edition.json");
        String expectedUpdateResponse = readFileAsString("outputs/pet/pet_edition_result.json");
        mockMvc.perform(put("/pet/1").contentType(MediaType.APPLICATION_JSON).content(petUpdate)).andExpect(status().isOk()).andExpect(content().json(expectedUpdateResponse, false));

        String expectedQuery = readFileAsString("outputs/pet/pet_initial.json");
        mockMvc.perform(get("/pet?page=0&pageSize=25&query=Adm")).andExpect(status().isOk()).andExpect(content().string(expectedQuery));
    }

}

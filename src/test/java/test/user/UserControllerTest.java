package test.user;

import static org.hamcrest.core.StringContains.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.puc.doggis.register.infraestructure.RegisterApplication;

import commons.AbstractTest;
import commons.ClearContext;

@RunWith (SpringRunner.class)
@SpringBootTest (classes = RegisterApplication.class)
@ClearContext
@WebAppConfiguration
public class UserControllerTest extends AbstractTest
{

    @Test
    @ClearContext
    public void testRetrieveAllUsersEmpty() throws Exception
    {
        String emptyUser = readFileAsString("outputs/client/client_initial.json");
        mockMvc.perform(get("/client")).andExpect(status().isOk()).andExpect(content().string(emptyUser));
    }

    @Test
    @ClearContext
    public void testCreateAndUpdateUser() throws Exception
    {
        String emptyUser = readFileAsString("outputs/client/client_initial.json");
        mockMvc.perform(get("/client")).andExpect(status().isOk()).andExpect(content().string(emptyUser));
        String clientCreation = readFileAsString("inputs/client/client_creation.json");
        String expectedCreationResponse = readFileAsString("outputs/client/client_creation_result.json");
        mockMvc.perform(post("/client").contentType(MediaType.APPLICATION_JSON).content(clientCreation)).andExpect(status().isCreated()).andExpect(content().json(expectedCreationResponse, false));

        String userUpdate = readFileAsString("inputs/client/client_edition.json");
        String expectedUpdateResponse = readFileAsString("outputs/client/client_edition_result.json");
        mockMvc.perform(put("/client/1").contentType(MediaType.APPLICATION_JSON).content(userUpdate)).andExpect(status().isOk()).andExpect(content().json(expectedUpdateResponse, false));

        String expectedQuery = readFileAsString("outputs/client/client_initial.json");
        mockMvc.perform(get("/client?page=0&pageSize=25&query=Adm")).andExpect(status().isOk()).andExpect(content().string(expectedQuery));
    }


}
